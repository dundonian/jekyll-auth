# Jekyll Auth Demo Site by Aerobatic

This site shows how to password protect a Jekyll site that's hosted with Aerobatic and uses Bitbucket Pipelines for continuous deployment.

The demo site is at https://jekyll-auth.aerobatic.io/